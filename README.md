# bson2json

Convert between json and bson data types.

## Usage

The `bson2json` module implements a `toJson` proc for `Bson` objects and a `toBson` proc for `JsonNode` objects, that recursively converts compatible object types between them both.

## Why?

Because nimongo is not compatible with JsonNode, as far as I can tell. :-(

Feel free to correct me over here: https://www.reddit.com/r/nim/comments/onxhd5/json_nimongo_data_exchange/

## Enjoy?

Responsibly!
