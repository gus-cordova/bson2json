# Package
description = "Convert between json and bson object types"
version     = "0.1"
license     = "MIT"
author      = "Gustavo Cordova <gustavo.cordova@gmail.com>"

# Dependencies
requires "nimongo >= 0.1"
